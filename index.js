const express = require("express");
const multer = require("multer");
const path = require("path");

const app = express();
const port = 8000;
const UPLOADES_FOLDER = "./uploades/";
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, UPLOADES_FOLDER);
  },
  filename: (req, file, cb) => {
    const exisTion = path.extname(file.originalname);
    const fileName = file.originalname.replace(exisTion, "").toLowerCase().split(" ").join("-") + "-" + Date.now() + exisTion;
    cb(null, fileName);
  },
});

const upload = multer({
  storage: storage,
  limits: 100000,
  fileFilter: (req, file, cb) => {
    if (file.fieldname === "profile") {
      if (file.mimetype === "image/png" || file.mimetype === "image/jpg" || file.mimetype === "image/jpeg") {
        cb(null, true);
      } else {
        cb(new Error("profile should be .png, .jpg and .jpeg"));
      }
    }
    if (file.fieldname === "images") {
      if (file.mimetype === "image/png" || file.mimetype === "image/jpg" || file.mimetype === "image/jpeg") {
        cb(null, true);
      } else {
        cb(new Error("images should be .png, .jpg and .jpeg"));
      }
    }
    if (file.fieldname === "doc") {
      if (file.mimetype === "application/pdf") {
        cb(null, true);
      } else {
        cb(new Error("Document should be pdf"));
      }
    }
  },
});

app.post(
  "/",
  upload.fields([
    { name: "profile", maxCount: 1 },
    { name: "images", maxCount: 2 },
    { name: "doc", maxCount: 2 },
  ]),
  (req, res) => {
    res.status(200).json(req.files);
  }
);

app.use((err, req, res, next) => {
  res.status(400).send(err.message);
});

app.listen(port, () => console.log(`Server is running on port ${port}`));
